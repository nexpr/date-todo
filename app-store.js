import { useState, useMemo } from "./standalone.module.js"
import util from "./util.js"

const dummy_default = [
	{
		date: "2020/01/01",
		todos: [
			{
				title: "dummy data",
				done: true,
				description: "description description description",
				history: [
					{
						text: "aaa",
						ts: 0,
					},
				],
			},
		],
	},
	{
		date: "2020/02/11",
		todos: [
			{
				title: "foo",
				done: true,
				description: "aa\nbb\ncc",
				history: [
					{
						text: "bbb",
						ts: 0,
					},
				],
			},
			{
				title: "bar",
				done: false,
				description: "text",
				history: [
					{
						text: "ccc",
						ts: 0,
					},
				],
			},
		],
	},
]

const initAppStore = () => {
	const state = {
		date_list: [],
		selected_date: null,
		selected_todo: null,
		dialog: null,
	}

	state.date_list = dummy_default

	if (localStorage.data) {
		try {
			state.date_list = JSON.parse(localStorage.data)
		} catch (err) {
			console.error("storage data was found, but it is invalid")
		}
	}

	let rerender = () => {}
	const bindRerender = (fn) => {
		rerender = fn
	}

	const updates = {
		"select-date"({ date }) {
			state.selected_date = date
			state.selected_todo = null
		},
		"change-status"({ todo, status }) {
			todo.done = status
			todo.history.push({
				ts: Date.now(),
				text: "status: " + (status ? "fninished" : "unfinished"),
			})
		},
		"select-todo"({ todo }) {
			state.selected_todo = todo
		},
		"unselect-todo"() {
			state.selected_todo = null
		},
		"add-todo"() {
			state.dialog = {
				text: "",
			}
		},
		"edit-todo"({ todo }) {
			state.dialog = {
				todo,
				text: todo.title + "\n\n" + todo.description,
			}
		},
		"delete-todo"({ todo }) {
			const index = state.selected_date.todos.indexOf(todo)
			if (index >= 0) {
				state.selected_date.todos.splice(index, 1)
				if (state.selected_todo === todo) {
					state.selected_todo = null
				}
				if (state.selected_date.todos.length === 0) {
					const i = state.date_list.indexOf(state.selected_date)
					state.date_list.splice(i, 1)
					state.selected_date = null
				}
			}
		},
		"input-dialog"({ text }) {
			if (!state.dialog) return
			state.dialog.text = text
		},
		"save-dialog"() {
			if (!state.dialog) return
			const { todo, text } = state.dialog
			const line_reader = util.lineReader(text)
			const line_first = line_reader.getLine()
			const line_rem = line_reader.get()

			const [title, description] = [line_first, line_rem].map((e) => e.trim())

			if (!title) {
				return
			}

			if (todo) {
				if (todo.title !== title || todo.description !== description) {
					todo.title = title
					todo.description = description
					todo.history.push({
						ts: Date.now(),
						text: "Updated",
					})
				}
			} else {
				const todo = {
					title,
					description,
					done: false,
					history: [
						{
							ts: Date.now(),
							text: "Created",
						},
					],
				}
				const today = util.dateFormat(new Date())
				const last = util.last(state.date_list)
				if (last && last.date === today) {
					last.todos.push(todo)
				} else {
					state.date_list.push({
						date: today,
						todos: [todo],
					})
				}
			}

			state.dialog = null
		},
		"cancel-dialog"() {
			state.dialog = null
		},
	}

	const update = (msg) => {
		const control = updates[msg.type](msg)
		localStorage.data = JSON.stringify(state.date_list)
		if (control && control.norender) return
		rerender()
	}

	return {
		state,
		update,
		bindRerender,
	}
}

const useAppStore = () => {
	const [state, setState] = useState({})
	const { bindRerender, ...data } = useMemo(initAppStore, [])

	bindRerender(() => setState({}))
	return data
}

export { useAppStore }
