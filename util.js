export default {
	dateFormat(d) {
		return [
			d.getFullYear(),
			String(d.getMonth() + 1).padStart(2, "0"),
			String(d.getDate()).padStart(2, "0"),
		].join("/")
	},

	last(x) {
		return x[x.length - 1]
	},

	lineReader(x) {
		let index = 0
		return {
			getLine() {
				const i = x.indexOf("\n", index)
				if (i < 0) {
					const result = x.slice(index)
					index = x.length
					return result
				} else {
					const result = x.slice(index, i)
					index = i + 1
					return result
				}
			},
			get() {
				return x.slice(index)
			}
		}
	}
}
