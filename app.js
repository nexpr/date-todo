import { html, render } from "./standalone.module.js"
import { useAppStore } from "./app-store.js"

const DateList = ({ selected_date, date_list, select }) => {
	return html`
		<div class="date-list">
			${date_list.map((date) => {
				const todo_count = date.todos.length
				const done_count = date.todos.filter((e) => e.done).length
				return html`
					<div
						class="date-item ${selected_date === date ? "selected" : ""}"
						key=${date.date}
						onclick=${() => select(date)}
					>
						<div class="date">${date.date}</div>
						<div class="todo-count">
							${`${done_count} / ${todo_count}`}
						</div>
					</div>
				`
			})}
		</div>
	`
}

const TodoContainer = ({ date, select, changeStatus }) => {
	return html`
		<div class="todo-container">
			<div class="todos">
				${date
					? date.todos.map((todo) => {
							return html`
								<div class="todo-item">
									<input
										type="checkbox"
										checked=${todo.done}
										onchange=${(eve) => changeStatus(todo, eve.target.checked)}
									/>
									<span onclick=${() => select(todo)}>${todo.title}</span>
								</div>
							`
					  })
					: "Please select date"}
			</div>
		</div>
	`
}

const SlideinDescription = ({ todo, unselectTodo, editTodo, deleteTodo }) => {
	return html`
		<div class="slidein-description ${todo ? "in" : ""}">
			${todo
				? html`
						<div class="top-buttons">
							<button onclick=${unselectTodo}>✖</button>
						</div>
						<div class="second-buttons">
							<button onclick=${() => editTodo(todo)}>Edit</button>
							<button onclick=${() => deleteTodo(todo)}>Delete</button>
						</div>
						<h1 class="title">${todo.title}</h1>
						<div class="description">${todo.description}</div>
						<h1 class="history">History</h1>
						<ul>
							${todo.history.map((h) => {
								return html`
									<li class="history-item">
										${new Date(h.ts).toLocaleString()} - ${h.text}
									</li>
								`
							})}
						</ul>
				  `
				: ""}
		</div>
	`
}

const EditDialog = ({ content, input, save, cancel }) => {
	return html`
		<div class="edit-dialog ${content ? "shown" : ""}">
			${content
				? html`
						<div class="dialog-inner">
							<div>
								<textarea value=${content.text} oninput=${(eve) => input(eve.target.value)}></textarea>
							</div>
							<div class="buttons">
								<button onclick=${() => save()}>${content.todo ? "Update" : "Add"}</button>
								<button onclick=${() => cancel()}>Cancel</button>
							</div>
						</div>
				  `
				: ""}
		</div>
	`
}

const App = () => {
	const { state, update } = useAppStore()

	return html`
		<header>
			<div class="left">Header</div>
			<div class="right">
				<button onclick=${() => update({ type: "add-todo" })}>Add</button>
			</div>
		</header>
		<div class="body">
			<${DateList}
				selected_date=${state.selected_date}
				date_list=${state.date_list}
				select=${(date) => update({ type: "select-date", date })}
			/>
			<${TodoContainer}
				date=${state.selected_date}
				select=${(todo) => update({ type: "select-todo", todo })}
				changeStatus=${(todo, status) => update({ type: "change-status", todo, status })}
			/>
			<${SlideinDescription}
				todo=${state.selected_todo}
				unselectTodo=${() => update({ type: "unselect-todo" })}
				editTodo=${(todo) => update({ type: "edit-todo", todo })}
				deleteTodo=${(todo) => update({ type: "delete-todo", todo })}
			/>
		</div>
		<${EditDialog}
			content=${state.dialog}
			input=${(text) => update({ type: "input-dialog", text })}
			save=${() => update({ type: "save-dialog" })}
			cancel=${() => update({ type: "cancel-dialog" })}
		/>
	`
}

render(html`<${App} />`, document.getElementById("root"))
